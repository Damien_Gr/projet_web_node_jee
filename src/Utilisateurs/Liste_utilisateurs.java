package Utilisateurs;

import Modele.RequeteSQL;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Liste_utilisateurs extends HttpServlet {

    RequeteSQL reqsql;

    public void doGet( HttpServletRequest request, HttpServletResponse response )	throws ServletException, IOException {
        this.getServletContext().getRequestDispatcher( "/restreint/liste-utilisateurs.jsp" ).forward( request, response );
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        this.getServletContext().getRequestDispatcher( "/restreint/liste-utilisateurs.jsp" ).forward( req, resp);
    }
}
