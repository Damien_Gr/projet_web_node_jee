package Etudiants;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

public class EtudiantForm {
    private static final String CHAMP_NOM  = "nom";
    private static final String CHAMP_PREN   = "prenom";
    private static final String CHAMP_NAISS  = "dateNaissance";
    private static final String CHAMP_COURPR   = "courielPro";
    private static final String CHAMP_COURPE  = "courielPerso";
    private static final String CHAMP_SERB  = "serieBac";
    private static final String CHAMP_DBAC  = "dateBac";
    private static final String CHAMP_MBAC   = "mentionBac";
    private static final String CHAMP_DIPL  = "diplome";
    private static final String CHAMP_DDIPL   = "dateDiplome";
    private static final String CHAMP_VILDIP  = "villeDiplome";


    private Map<String, String> erreurs      = new HashMap<String, String>();

    public EtudiantForm(){}

    public Map<String, String> getErreurs() {
        return erreurs;
    }

    public  Etudiants Recup(HttpServletRequest request){

        /* Récupération des champs du formulaire */
        String nom = getValeurChamp( request, CHAMP_NOM );
        System.out.println(nom);
        String prenom = getValeurChamp( request, CHAMP_PREN );
        System.out.println(prenom);
        String naiss = getValeurChamp( request, CHAMP_NAISS);
        System.out.println(naiss);
        String cpro = getValeurChamp( request, CHAMP_COURPR);
        System.out.println(cpro);
        String cper = getValeurChamp( request, CHAMP_COURPE);
        System.out.println(cper);
        String serb = getValeurChamp( request, CHAMP_SERB);
        System.out.println(serb);
        String datBac = getValeurChamp( request, CHAMP_DBAC);
        System.out.println(datBac);
        String mentBac = getValeurChamp( request, CHAMP_MBAC);
        System.out.println(mentBac);
        String dipl = getValeurChamp( request, CHAMP_DIPL);
        System.out.println(dipl);
        String ddipl = getValeurChamp( request, CHAMP_DDIPL);
        System.out.println(ddipl);
        String vdipl = getValeurChamp( request, CHAMP_VILDIP);
        System.out.println(vdipl);

        Etudiants E = new Etudiants(1,nom,prenom, naiss,cpro,cper,serb,datBac,mentBac,dipl,ddipl,vdipl);
        System.out.println("rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr");
        System.out.println(E.courielPerso);
        return E;
    }

    private static String getValeurChamp( HttpServletRequest request, String nomChamp ) {
        String valeur = request.getParameter( nomChamp );
        if ( valeur == null || valeur.trim().length() == 0 ) {
            return null;
        } else {
            return valeur;
        }
    }


}
