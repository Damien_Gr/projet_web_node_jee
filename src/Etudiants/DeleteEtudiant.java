package Etudiants;

import Modele.RequeteSQL;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DeleteEtudiant extends HttpServlet {

    RequeteSQL reqsql = new RequeteSQL();
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // TODO Auto-generated method stub
        String id =  request.getParameter( "ID");
        request.setAttribute("ID", id );
        this.getServletContext().getRequestDispatcher("/restreint/delete-etudiant.jsp").forward(request, response);

    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        /* String action = req.getParameter("action");
        System.out.println("Do Post");
        System.out.println(action);
        switch (action) {
            case "sauvegarder": //guardar
              //  sauvegarderEtudiant(req, resp);
                break;
            case "actualiser": //actualizar
                //actualiserEtudiant(req, resp);
                break;
            case "supprimer": //eliminar
                //supprimerEtudiant(req, resp);
                break;
        }*/
        String id =  (String) req.getParameter( "ID");
        System.out.println("DELETE ID = " + id);
        Etudiants etu = reqsql.selectEtudiantId(id);
        System.out.println("nom = " + etu.getNom());
        req.setAttribute("ETU", etu );
        this.getServletContext().getRequestDispatcher("/restreint/delete-etudiant.jsp").forward(req, resp);

    }
}

   /* private void eliminarEstudiante(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        long idEtudiant = Integer.valueOf(req.getParameter("idUtilisateur"));
        //boolean confirmar = estudianteDAO.eliminarEstudiante(idEtudiant);
        //if (confirmar){
            String message = "Registro eliminado satisfactoriamente.";
            req.setAttribute("message", message);
        }
        //List<Etudiants> listaEstudiantes = estudianteDAO.obtenerEstudiantes();
       // mostrarListaEstudiantes(req, resp, listaEstudiantes);
    } */

//}