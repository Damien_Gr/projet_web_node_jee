package Etudiants;

public class Etudiants {
    int idUtilisateur;
    String nom;
    String prenom;
    String dateNaissance;
    String courielPro;
    String  courielPerso ;
    String serieBac ;
    String dateBac;
    String mentionBac;
    String diplome;
    String dateDiplome ;
    String villeDiplome ;

    public Etudiants(int idUtilisateur, String nom, String prenom, String dateNaissance, String courielPro, String courielPerso, String serieBac, String dateBac, String mentionBac, String diplome, String dateDiplome, String villeDiplome) {
        this.idUtilisateur = idUtilisateur;
        this.nom = nom;
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;
        this.courielPro = courielPro;
        this.courielPerso = courielPerso;
        this.serieBac = serieBac;
        this.dateBac = dateBac;
        this.mentionBac = mentionBac;
        this.diplome = diplome;
        this.dateDiplome = dateDiplome;
        this.villeDiplome = villeDiplome;
    }

    public Etudiants() {

    }

    @Override
    public String toString() {
        return "Etudiants{" +
                "idUtilisateur=" + idUtilisateur +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", dateNaissance='" + dateNaissance + '\'' +
                ", courielPro='" + courielPro + '\'' +
                ", courielPerso='" + courielPerso + '\'' +
                ", serieBac='" + serieBac + '\'' +
                ", dateBac='" + dateBac + '\'' +
                ", mentionBac='" + mentionBac + '\'' +
                ", diplome='" + diplome + '\'' +
                ", dateDiplome='" + dateDiplome + '\'' +
                ", villeDiplome='" + villeDiplome + '\'' +
                '}';
    }

    public int getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(int idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(String dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getCourielPro() {
        return courielPro;
    }

    public void setCourielPro(String courielPro) {
        this.courielPro = courielPro;
    }

    public String getCourielPerso() {
        return courielPerso;
    }

    public void setCourielPerso(String courielPerso) {
        this.courielPerso = courielPerso;
    }

    public String getSerieBac() {
        return serieBac;
    }

    public void setSerieBac(String serieBac) {
        this.serieBac = serieBac;
    }

    public String getDateBac() {
        return dateBac;
    }

    public void setDateBac(String dateBac) {
        this.dateBac = dateBac;
    }

    public String getMentionBac() {
        return mentionBac;
    }

    public void setMentionBac(String mentionBac) {
        this.mentionBac = mentionBac;
    }

    public String getDiplome() {
        return diplome;
    }

    public void setDiplome(String diplome) {
        this.diplome = diplome;
    }

    public String getDateDiplome() {
        return dateDiplome;
    }

    public void setDateDiplome(String dateDiplome) {
        this.dateDiplome = dateDiplome;
    }

    public String getVilleDiplome() {
        return villeDiplome;
    }

    public void setVilleDiplome(String villeDiplome) {
        this.villeDiplome = villeDiplome;
    }
}