package Etudiants;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class EtudiantCree extends HttpServlet {

    public void doGet( HttpServletRequest request, HttpServletResponse response )	throws ServletException, IOException {
        this.getServletContext().getRequestDispatcher( "/restreint/etudiant-cree.jsp" ).forward( request, response );
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        this.getServletContext().getRequestDispatcher( "/restreint/etudiant-cree.jsp" ).forward( req, resp);
    }

//    private void validationMotsDePasse(String username, String password) {
//    }
}
