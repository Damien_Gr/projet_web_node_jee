package Etudiants;

import Modele.RequeteSQL;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class NouvelEtudiant extends HttpServlet {

    RequeteSQL reqsql = new RequeteSQL();
    public static final String ATT_FORM         = "form";

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // TODO Auto-generated method stub

        this.getServletContext().getRequestDispatcher("/restreint/nouvel-etudiant.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //Etudiants E = new Etudiants();

        //request.setAttribute("etudiant-cree", E);

        EtudiantForm form = new EtudiantForm();

        Etudiants etu = form.Recup(request);

        request.setAttribute( ATT_FORM, form );
        request.setAttribute( "etudiant", etu );


        reqsql.insertEtudiant(etu.getNom(), etu.getPrenom(), etu.getDateNaissance(), etu.getCourielPerso(), etu.getCourielPerso(),etu.getSerieBac(),
                etu.getDateBac(), etu.getMentionBac(), etu.getDiplome(), etu.getDateDiplome(), etu.getVilleDiplome());

        this.getServletContext().getRequestDispatcher("/restreint/etudiant-cree").forward(request, response);

    }
}

