package Etudiants;

import Modele.RequeteSQL;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class Liste_etudiants extends HttpServlet {

    public static final String ATT_LIST      = "EtuList";
    RequeteSQL reqsql = new RequeteSQL();

    public void doGet( HttpServletRequest request, HttpServletResponse response )	throws ServletException, IOException {

        List L =  reqsql.selectEtudiantsTout();
        System.out.println(L);
        request.setAttribute(ATT_LIST, L);
        this.getServletContext().getRequestDispatcher( "/restreint/liste-etudiants.jsp" ).forward( request, response );

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String id = (String)req.getParameter("ID");
        System.out.println("Supprime ID = " + id );
        reqsql.deleteEtudiant(id);
        List L =  reqsql.selectEtudiantsTout();
        System.out.println(L);
        req.setAttribute(ATT_LIST, L);
        this.getServletContext().getRequestDispatcher( "/restreint/liste-etudiants.jsp" ).forward( req, resp);
    }
}
