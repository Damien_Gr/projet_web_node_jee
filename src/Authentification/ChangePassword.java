package Authentification;

import Session.PasswordChangeForm;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ChangePassword extends HttpServlet {

    public static final String VUE              = "/changePassword.jsp" ;
    public static final String VUE2             = "/changePasswordSuccess.jsp";
    public static final String ATT_FORM         = "form";

    public void doGet( HttpServletRequest request, HttpServletResponse response )	throws ServletException, IOException {
        this.getServletContext().getRequestDispatcher( "/changePassword.jsp" ).forward( request, response );
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        /* Pr�paration de l'objet formulaire */
        PasswordChangeForm form = new PasswordChangeForm();

        /* Traitement de la requ�te */
        form.changerMotDePasse( req );

        req.setAttribute( ATT_FORM, form );

        /**
         * Si aucune erreur de validation n'a eu lieu, alors ajout du bean
         * Utilisateur � la session, sinon suppression du bean de la session.
         */
        if (form.getErreurs().isEmpty()) {
            this.getServletContext().getRequestDispatcher( VUE2 ).forward( req, resp );
        } else {
            this.getServletContext().getRequestDispatcher( VUE ).forward( req, resp );
        }
    }

}
