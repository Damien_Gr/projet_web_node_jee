package Session;

import Modele.RequeteSQL;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;


public final class LoginForm {

    private static final String CHAMP_EMAIL  = "email";
    private static final String CHAMP_PASS   = "password";

    private RequeteSQL reqsql = new RequeteSQL();
    private boolean              resultat;
    private Map<String, String> erreurs      = new HashMap<String, String>();

    public LoginForm() {

    }

    public boolean getResultat() {
        return resultat;
    }

    public Map<String, String> getErreurs() {
        return erreurs;
    }

    public Utilisateur connecterUtilisateur( HttpServletRequest request ) {

        /* Récupération des champs du formulaire */
        String email = getValeurChamp( request, CHAMP_EMAIL );
        System.out.println(email);
        String motDePasse = getValeurChamp( request, CHAMP_PASS );
        System.out.println(motDePasse);


        /* Validation de l'utlisateur */
        Utilisateur utilisateur = null;
        try {
            utilisateur = validationUtilisateur(email, motDePasse);
        } catch (Exception e) {
            setErreur( "Utilisateur", e.getMessage() );
        }

        /* Initialisation du résultat global de la validation. */
        System.out.println(erreurs);
        System.out.println(utilisateur);

        if ( erreurs.isEmpty() ) {
            resultat = true;
        } else {
            resultat = false;
        }

        return utilisateur;
    }

    private Utilisateur validationUtilisateur(String email, String motDePasse) throws Exception {

        if (reqsql.isUser(email,motDePasse) ) {
            Utilisateur U = reqsql.getUser(email);
            System.out.println(U.getNom());
            return U;
        }else {

            throw new Exception("Utilisateur inconnu");
        }
    }


    /*
     * Ajoute un message correspondant au champ spécifié à la map des erreurs.
     */
    private void setErreur( String champ, String message ) {
        erreurs.put( champ, message );
    }

    /*
     * Méthode utilitaire qui retourne null si un champ est vide, et son contenu sinon.
     */
    private static String getValeurChamp( HttpServletRequest request, String nomChamp ) {
        String valeur = request.getParameter( nomChamp );
        if ( valeur == null || valeur.trim().length() == 0 ) {
            return null;
        } else {
            return valeur;
        }
    }
}