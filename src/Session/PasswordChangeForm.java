package Session;

import Modele.RequeteSQL;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;


public final class PasswordChangeForm {

    private static final String CHAMP_EMAIL  = "email";
    private static final String CHAMP_REP   = "reponse";
    private static final String CHAMP_PASS  = "password";
    private static final String CHAMP_PASS2  = "password2";

    private RequeteSQL reqSQL = new RequeteSQL();

    private boolean              resultat;
    private Map<String, String> erreurs      = new HashMap<String, String>();

    public Map<String, String> getErreurs() {
        return erreurs;
    }

    public void changerMotDePasse( HttpServletRequest request ) {

        /* Récupération des champs du formulaire */
        String email = getValeurChamp( request, CHAMP_EMAIL );
        String reponse = getValeurChamp( request, CHAMP_REP );
        String password = getValeurChamp( request, CHAMP_PASS );
        String password2 = getValeurChamp( request, CHAMP_PASS2 );

        /* Validation du changement de mot de passe */
        try {
            if (!password.equals(password2)){
                throw new Exception("Saisie incorrecte");
            }
            if (!(reqSQL.changementMDP(email,reponse,password))) {
                        throw new Exception("Saisie incorrecte");
                    }
            //méthode pour essayer de changer le mot de passe
        } catch (Exception e) {
            setErreur( "Utilisateur", e.getMessage() );
        }


        if ( erreurs.isEmpty() ) {
            resultat = true;
        } else {
            resultat = false;
        }
    }



    /*
     * Ajoute un message correspondant au champ spécifié à la map des erreurs.
     */
    private void setErreur( String champ, String message ) {
        erreurs.put( champ, message );
    }

    /*
     * Méthode utilitaire qui retourne null si un champ est vide, et son contenu sinon.
     */
    private static String getValeurChamp( HttpServletRequest request, String nomChamp ) {
        String valeur = request.getParameter( nomChamp );
        if ( valeur == null || valeur.trim().length() == 0 ) {
            return null;
        } else {
            return valeur;
        }
    }
}