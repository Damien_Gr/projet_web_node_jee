
#Connextion � mysql
#�mysql -h localhost -p

# Cr�ation de la base de donn�e

CREATE DATABASE bdJEE DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;


# Cr�ation d un utilisateur de la DATABASE

CREATE USER 'java'@'localhost' IDENTIFIED BY 'coucou';
GRANT ALL ON bdJEE.* TO 'java'@'localhost' IDENTIFIED BY 'coucou';

# exit la bdd et relancer avec
#  mysql -h localhost -u java -p
use bdJEE;

#Cr�ation de la table Users

CREATE TABLE  bdJEE.Users (
 id INT NOT NULL AUTO_INCREMENT ,
 nom VARCHAR( 32 ) NOT NULL ,
 prenom VARCHAR( 32 ) NOT NULL ,
 mail varchar( 40 ) NOT NULL,
 mdp VARCHAR( 32 ) NOT NULL ,
 statut VARCHAR( 20 ) NOT NULL,
 PRIMARY KEY (id)
) ENGINE = INNODB;

INSERT INTO Users (nom, prenom, mail, mdp, statut) VALUES ('Menneret', 'Bryan', 'bryan@mail.fr', MD5('bcoucou'), 'admin');
INSERT INTO Users (nom, prenom, mail, mdp, statut) VALUES ('Betrom', 'Guillaume', 'guillaume@mail.fr',  MD5('gcoucou'), 'admin');
INSERT INTO Users (nom, prenom, mail, mdp, statut) VALUES ('Gramusset', 'Damien', 'damien@mail.fr',  MD5('dcoucou'), 'admin');
INSERT INTO Users (nom, prenom, mail, mdp, statut) VALUES ('Auroyer', 'Vincent', 'vincent@mail.fr',  MD5('vcoucou'), 'admin');
INSERT INTO Users (nom, prenom, mail, mdp, statut) VALUES ('Martin', 'Julian', 'julian@mail.fr',  MD5('jcoucou'), 'admin');

# Cr�ation de la table Etudiants

CREATE TABLE  bdJEE.Etudiants (
 id INT NOT NULL AUTO_INCREMENT ,
 nom VARCHAR( 32 ) NOT NULL ,
 prenom VARCHAR( 32 ) NOT NULL ,
 dateNaissance DATE NOT NULL,
 courielPro VARCHAR( 32 ),
 courielPerso VARCHAR( 32 ),
 serieBac VARCHAR( 32 ) NOT NULL,
 dateBac DATE NOT NULL,
 mentionBac VARCHAR( 32 ),
 diplome VARCHAR( 32 ),
 dateDiplome VARCHAR( 32 ),
 villeDiplome VARCHAR( 32 ),
 PRIMARY KEY (ID)
) ENGINE = INNODB;
