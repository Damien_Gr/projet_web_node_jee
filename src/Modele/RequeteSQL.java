package Modele;

import Etudiants.Etudiants;
import Session.Utilisateur;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;


public class RequeteSQL {


    public RequeteSQL() {
    }



    private final String url = "jdbc:mysql://localhost:3306/bdJEE?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&characterEncoding=latin1&useConfigs=maxPerformance";
    private final String utilisateur = "java";
    private final String motDePasse = "coucou";

    //return ResultSet
    private Resultat execSQLRead(String query) {
        try {
            System.out.println("Chargement du driver...");
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Driver chargé !");
        } catch (ClassNotFoundException e) {
            System.out.println("Erreur lors du chargement : le driver n'a pas été trouvé dans le classpath ! <br/>"
                    + e.getMessage());
        }

        /* Connexion à la base de données */
        Connection connexion = null;
        Statement statement = null;
        ResultSet resultat = null;

        try {
            System.out.println("Connexion à la base de données...");
            connexion = DriverManager.getConnection(url, utilisateur, motDePasse);
            System.out.println("Connexion réussie !");

            /* Création de l'objet gérant les requêtes */
            statement = connexion.createStatement();
            System.out.println("Objet requête créé !");

            /* Exécution d'une requéte de lecture */
            resultat = statement.executeQuery(query);
            System.out.println("Requête " + query + " effectuée !");

        } catch (SQLException e) {
            System.out.println("Erreur lors de la connexion : <br/>"
                    + e.getMessage());
        }

        Resultat res = new Resultat(resultat,statement,connexion);

        System.out.println("Fermeture de l'objet Statement.");

        return res;
    }

    //return status
    private int execSQLWrite(String query) {
        System.out.println(query);
        try {
            System.out.println("Chargement du driver...");
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Driver chargé !");
        } catch (ClassNotFoundException e) {
            System.out.println("Erreur lors du chargement : le driver n'a pas été trouvé dans le classpath ! <br/>"
                    + e.getMessage());
        }

        /* Connexion à la base de données */
        Connection connexion = null;
        Statement statement = null;
        ResultSet resultat = null;
        int statut = 0;

        try {
            System.out.println("Connexion à la base de données...");
            connexion = DriverManager.getConnection(url, utilisateur, motDePasse);
            System.out.println("Connexion réussie !");

            /* Création de l'objet gérant les requêtes */
            statement = connexion.createStatement();
            System.out.println("Objet requête créé !");


            /* Exécution d'une requête d'écriture */
            statut = statement.executeUpdate(query);
            System.out.println("Requête : " + query);
            System.out.println("Statut de la requête = " + statut);

        } catch (SQLException e) {
            System.out.println("Erreur lors de la connexion : <br/>"
                    + e.getMessage());
        }

        System.out.println("Fermeture de l'objet Statement.");
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException ignore) {
            }
        }
        System.out.println("Fermeture de l'objet Connection.");
        if (connexion != null) {
            try {
                connexion.close();
            } catch (SQLException ignore) {
            }
        }

        return statut;
    }

    //SELECT * FROM Etudiants
    public List<Etudiants> selectEtudiantsTout() {


        Resultat res = execSQLRead("SELECT * FROM Etudiants;");
        ResultSet resultat = res.resultSet;
        if (resultat == null){
            System.out.println("resultat est null");
        }

        System.out.println("ANIUDKJHNHLIK: ");
        List<Etudiants> l = new LinkedList<Etudiants>();

        try {
            /* Recuperation des donnees du resultat de la requête de lecture */
            while (resultat.next()) {
                l.add(new Etudiants(resultat.getInt("id"), resultat.getString("nom"), resultat.getString("prenom"), resultat.getString("dateNaissance"), resultat.getString("courielPro"), resultat.getString("courielPerso"), resultat.getString("serieBac"), resultat.getString("dateBac"), resultat.getString("mentionBac"), resultat.getString("diplome"), resultat.getString("dateDiplome"), resultat.getString("villeDiplome")));
            }
        } catch (SQLException e) {
            System.out.println("Erreur lors de la lecture du ResultSet : "
                    + e.getMessage());
        } finally {
            System.out.println("Fermeture de l'objet ResultSet.");
            if (resultat != null) {
                try {
                    resultat.close();
                } catch (SQLException ignore) {
                }
            }
        }

        if (res.statement != null) {
            try {
                res.statement.close();
            } catch (SQLException ignore) {
            }
        }
        System.out.println("Fermeture de l'objet Connection.");
        if (res.connection != null) {
            try {
                res.connection.close();
            } catch (SQLException ignore) {
            }
        }

        return l;
    }

    //SELECT id, nom ,prenom FROM Etudiants;
    public List selectEtudiants() {

        /* Exécution d'une requête de lecture */
        Resultat res = execSQLRead("SELECT id, nom ,prenom FROM Etudiants;");
        ResultSet resultat = res.resultSet;
        if (resultat == null){
            System.out.println("resultat est null");
        }

        List l = new LinkedList();

        try {
            /* Recuperation des donnees du resultat de la requête de lecture */
            while (resultat.next()) {
                l.add(resultat.getInt("id"));
                l.add(resultat.getString("nom"));
                l.add(resultat.getString("prenom"));
            }
        } catch (SQLException e) {
            System.out.println("Erreur lors de la lecture du ResultSet : "
                    + e.getMessage());
        } finally {
            System.out.println("Fermeture de l'objet ResultSet.");
            if (resultat != null) {
                try {
                    resultat.close();
                } catch (SQLException ignore) {
                }
            }
        }
        return l;
    }

    //SELECT * FROM Etudiants WHERE nom = in_nom, prenom = in_prenom
    public List selectEtudiant(String in_nom, String in_prenom) {


        /* Exécution d'une requête de lecture */
        Resultat res = execSQLRead("SELECT * FROM Etudiants WHERE nom = \'" + in_nom + "\' and prenom = \'" + in_prenom + "\' ;");


        ResultSet resultat = res.resultSet;
        if (resultat == null){
            System.out.println("resultat est null");
        }

        List l = new LinkedList();

        try {
            /* Recuperation des donnees du resultat de la requête de lecture */
            while (resultat.next()) {
                l.add(resultat.getInt("id"));
                l.add(resultat.getString("nom"));
                l.add(resultat.getString("prenom"));
                l.add(resultat.getDate("dateNaissance"));
                l.add(resultat.getString("courielPro"));
                l.add(resultat.getString("courielPerso"));
                l.add(resultat.getString("serieBac"));
                l.add(resultat.getDate("dateBac"));
                l.add(resultat.getString("mentionBac"));
                l.add(resultat.getString("diplome"));
                l.add(resultat.getDate("dateDiplome"));
                l.add(resultat.getString("villeDiplome"));
            }
        } catch (SQLException e) {
            System.out.println("Erreur lors de la lecture du ResultSet : "
                    + e.getMessage());
        } finally {
            System.out.println("Fermeture de l'objet ResultSet.");
            if (resultat != null) {
                try {
                    resultat.close();
                } catch (SQLException ignore) {
                }
            }
        }
        return l;
    }

    public Etudiants selectEtudiantId(String Id) {


        /* Exécution d'une requête de lecture */
        Resultat res = execSQLRead("SELECT * FROM Etudiants WHERE id = \'" + Id + "\' ;");
        Etudiants Etu = null ;

        ResultSet resultat = res.resultSet;
        if (resultat == null){
            System.out.println("resultat est null");
        }



        try {
            /* Recuperation des donnees du resultat de la requête de lecture */
            if (resultat.next()) {
                 Etu = new Etudiants(resultat.getInt("id"), resultat.getString("nom"), resultat.getString("prenom"), resultat.getString("dateNaissance"), resultat.getString("courielPro"), resultat.getString("courielPerso"), resultat.getString("serieBac"), resultat.getString("dateBac"), resultat.getString("mentionBac"), resultat.getString("diplome"), resultat.getString("dateDiplome"), resultat.getString("villeDiplome"));
            }
        } catch (SQLException e) {
            System.out.println("Erreur lors de la lecture du ResultSet : "
                    + e.getMessage());
        } finally {
            System.out.println("Fermeture de l'objet ResultSet.");
            if (resultat != null) {
                try {
                    resultat.close();
                } catch (SQLException ignore) {
                }
            }
        }
        return Etu;
    }

    //DELETE FROM Etudiants WHERE nom, prenom, dateNaissance = "+in_nom+", "+in_prenom+", "+in_dateN+"
    public void deleteEtudiant(String in_id) {

        System.out.println("deleteEtudiant =" + in_id);
        /* Exécution d'une requête d'écriture */
        int statut = execSQLWrite("DELETE FROM Etudiants WHERE ID = \'" + in_id + "\';");

    }

    //INSERT INTO ETUDIANTS(...)
    public void insertEtudiant(String in_nom, String in_prenom, String in_dateN, String in_mailPr, String in_mailPerso, String in_serieBac,
                               String in_dateBac, String in_mentionBac, String in_diplome, String in_dateDiplome, String in_villeDiplome) {
        System.out.println("JE PASSE PAR LA MEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEC");
        /* Exécution d'une requête d'écriture */
        int statut = execSQLWrite("INSERT INTO Etudiants (nom, prenom, dateNaissance, courielPro, courielPerso, serieBac, dateBac, mentionBac, diplome, dateDiplome,villeDiplome) VALUES (\'" + in_nom + "\', \'" + in_prenom + "\',\' " + in_dateN + "\', \'" + in_mailPr + "\',\' " + in_mailPerso + "\',\'" + in_serieBac + "\', \'" + in_dateBac + "\', \'" + in_mentionBac + "\', \'" + in_diplome + "\', \'" + in_dateDiplome + "\', \'" + in_villeDiplome + "\');");

    }

    public void modifEtudiant(int in_id,String in_nom, String in_prenom, String in_dateN, String in_mailPr, String in_mailPerso, String in_serieBac,
                               String in_dateBac, String in_mentionBac, String in_diplome, String in_dateDiplome, String in_villeDiplome) {
        System.out.println("MODIF ETUDIANT");
        /* Exécution d'une requête d'écriture */
        int statut = execSQLWrite("UPDATE Etudiants SET nom = \'" + in_nom + "\', prenom = \'" + in_prenom + "\', dateNaissance = \'" + in_dateN +"\' , courielPro = \'" + in_mailPr +"\' , courielPerso = \'" + in_mailPerso +"\' , serieBac = \'" + in_serieBac + "\' , dateBac = \'" + in_dateBac +"\' , mentionBac = \'" + in_mentionBac + "\' , dateDiplome = \'" +in_dateDiplome +"\' ,villeDiplome = \'" + in_villeDiplome +"\' WHERE id = "+in_id + ";");
        //int statut = execSQLWrite("UPDATE Etudiants SET (nom, prenom, dateNaissance, courielPro, courielPerso, serieBac, dateBac, mentionBac, diplome, dateDiplome,villeDiplome) = (\'" + in_nom + "\', \'" + in_prenom + "\',\' " + in_dateN + "\', \'" + in_mailPr + "\',\' " + in_mailPerso + "\',\'" + in_serieBac + "\', \'" + in_dateBac + "\', \'" + in_mentionBac + "\', \'" + in_diplome + "\', \'" + in_dateDiplome + "\', \'" + in_villeDiplome + "\') WHERE id = "+in_id+";");


    }

    //Vérification utilisateur return boolean
    public boolean isUser(String in_mail, String in_mdp) {
        String nom = null;

        /* Exécution d'une requête de lecture */
        Resultat res = execSQLRead("SELECT * FROM Users WHERE mail = \'" + in_mail + "\' and mdp = \'" + in_mdp + "\' ;");


        //Connection connexion = res.connection;
        //Statement statement = res.statement;
        ResultSet resultat = res.resultSet;

        try {
            /* Recuperation des donnees du resultat de la requête de lecture */
            if (resultat.next()) {
                nom = resultat.getString(2);
                System.out.println(nom);
            }
        } catch (SQLException e) {
            System.out.println("Erreur lors de la lecture du ResultSet : "
                    + e.getMessage());
        } finally {
            System.out.println("Fermeture de l'objet ResultSet.");
            if (resultat != null) {
                try {
                    resultat.close();
                } catch (SQLException ignore) {
                }
            }
        }


        if (res.statement != null) {
            try {
                res.statement.close();
            } catch (SQLException ignore) {
            }
        }
        System.out.println("Fermeture de l'objet Connection.");
        if (res.connection != null) {
            try {
                res.connection.close();
            } catch (SQLException ignore) {
            }
        }

        if (nom != null){
            return true;
        } else {
            return false;
        }
    }

    //UPDATE ON USER
    public Utilisateur getUser(String in_mail) {
        /* Exécution d'une requête de lecture */
        Resultat res = execSQLRead("SELECT * FROM Users WHERE mail = \'" + in_mail + "\' ;");
        ResultSet resultat = res.resultSet;

        String nom = null;
        String prenom = null;
        String email = in_mail;
        String statut = null;

        Utilisateur user = null;

        try {

            if (resultat.next()) {
                nom = resultat.getString("nom");
                prenom = resultat.getString("prenom");
                statut = resultat.getString("statut");
                user = new Utilisateur(nom,prenom,email,statut);

            }
        } catch (SQLException e) {
            System.out.println("Erreur lors de la lecture du ResultSet : "
                    + e.getMessage());
        } finally {
            System.out.println("Fermeture de l'objet ResultSet.");
            if (resultat != null) {
                try {
                    resultat.close();
                } catch (SQLException ignore) {
                }
            }
        }


        if (res.statement != null) {
            try {
                res.statement.close();
            } catch (SQLException ignore) {
            }
        }
        System.out.println("Fermeture de l'objet Connection.");
        if (res.connection != null) {
            try {
                res.connection.close();
            } catch (SQLException ignore) {
            }
        }
        return user;


    }

    //Changement de mdp return boolean
    public boolean changementMDP(String in_mail, String in_reponse,String in_Nmdp) throws Exception{
        String nom = null;


        //////////// Partie identification ///////////////////////////////
        /* Exécution d'une requête de lecture */
        Resultat res = execSQLRead("SELECT * FROM Users WHERE mail = \'" + in_mail + "\' and reponse = \'" + in_reponse + "\' ;");
        //Connection connexion = res.connection;
        //Statement statement = res.statement;
        ResultSet resultat = res.resultSet;
        try {
            /* Recuperation des donnees du resultat de la requête de lecture */
            if (resultat.next()) {
                nom = resultat.getString(2);
                System.out.println(nom);
            }
        } catch (SQLException e) {
            System.out.println("Erreur lors de la lecture du ResultSet : "
                    + e.getMessage());
        } finally {
            System.out.println("Fermeture de l'objet ResultSet.");
            if (resultat != null) {
                try {
                    resultat.close();
                } catch (SQLException ignore) {
                }
            }
        }

        /////////////////////// Partie mise à jour de la base ////////////////////
        if(nom == null){
            return false;
        } else {
            int statut = execSQLWrite("UPDATE Users SET mdp = \'"+ in_Nmdp +"\' WHERE mail = \'" + in_mail + "\' and reponse = \'" + in_reponse + "\';");
            System.out.println("Statut de la requte :" +statut);
            return true;
        }

    }

    //Changement de statut
    public void changementStatut(String in_mail, String in_statut) throws Exception{

        int statut = execSQLWrite("UPDATE Users SET statut = \'"+ in_statut +"\' WHERE mail = \'" + in_mail + "\' ;");
        System.out.println("Statut de la requette :" +statut);

    }

    //SELECT * FROM Users WHERE mail = in_mail
    public List selectUser(String in_mail) {


        /* Exécution d'une requête de lecture */
        Resultat res = execSQLRead("SELECT * FROM Users WHERE mail = \'" + in_mail +"\' ;");


        ResultSet resultat = res.resultSet;
        if (resultat == null){
            System.out.println("resultat est null");
        }

        List l = new LinkedList();

        try {
            /* Recuperation des donnees du resultat de la requête de lecture */
            while (resultat.next()) {
                l.add(resultat.getInt("id"));
                l.add(resultat.getString("nom"));
                l.add(resultat.getString("prenom"));
                l.add(resultat.getString("statut"));
                l.add(resultat.getString("mail"));
            }
        } catch (SQLException e) {
            System.out.println("Erreur lors de la lecture du ResultSet : "
                    + e.getMessage());
        } finally {
            System.out.println("Fermeture de l'objet ResultSet.");
            if (resultat != null) {
                try {
                    resultat.close();
                } catch (SQLException ignore) {
                }
            }
        }
        return l;
    }

    //CREAT TABLE bdJEE."nom_table"(id INT NOT NULL)
    public void createGroupe(String in_nomGroupe){

        /* Exécution d'une requête d'écriture */
        int statut = execSQLWrite("CREATE TABLE bdJEE."+in_nomGroupe+" (id INT NOT NULL, PRIMARY KEY(id)) ENGINE = INNODB;");
        System.out.println("Satut : " +statut);
    }

    //INSERT INTO in_nomGroupe id VALUES in_id
    public void insertEtudiantsGroupe(String in_nomGroupe, List in_ids){
        int i = 0;
        while (i <in_ids.size()){
            int statut = execSQLWrite("INSERT INTO (" +in_nomGroupe+ ") id VALUES (" +in_ids.get(i)+ ");");
            i++;
            System.out.println("Statut : " +statut);
        }
    }

    //SELECT * FROM in_nomGroupe
    public List selectEtudiantsGroupeTout(String in_nomGroupe){

        Resultat res = execSQLRead("SELECT * FROM Etudiants WHERE id = (SELECT id FROM "+in_nomGroupe+");");
        ResultSet resultat = res.resultSet;
        if (resultat == null){
            System.out.println("resultat est null");
        }

        List l = new LinkedList();

        try {
            /* Recuperation des donnees du resultat de la requête de lecture */
            while (resultat.next()) {
                l.add(resultat.getInt("id"));
                l.add(resultat.getString("nom"));
                l.add(resultat.getString("prenom"));
                l.add(resultat.getDate("dateNaissance"));
                l.add(resultat.getString("courielPro"));
                l.add(resultat.getString("courielPerso"));
                l.add(resultat.getString("serieBac"));
                l.add(resultat.getDate("dateBac"));
                l.add(resultat.getString("mentionBac"));
                l.add(resultat.getString("diplome"));
                l.add(resultat.getDate("dateDiplome"));
                l.add(resultat.getString("villeDiplome"));
            }
        } catch (SQLException e) {
            System.out.println("Erreur lors de la lecture du ResultSet : "
                    + e.getMessage());
        } finally {
            System.out.println("Fermeture de l'objet ResultSet.");
            if (resultat != null) {
                try {
                    resultat.close();
                } catch (SQLException ignore) {
                }
            }
        }
        return l;
    }

    //SELECT id, nom, prenom FROM in_nomGroupe
    public List selectEtudiantsGroupe(String in_nomGroupe){

        Resultat res = execSQLRead("SELECT * FROM Etudiants WHERE id = (SELECT id FROM "+in_nomGroupe+");");
        ResultSet resultat = res.resultSet;
        if (resultat == null){
            System.out.println("resultat est null");
        }

        List l = new LinkedList();

        try {
            /* Recuperation des donnees du resultat de la requête de lecture */
            while (resultat.next()) {
                l.add(resultat.getInt("id"));
                l.add(resultat.getString("nom"));
                l.add(resultat.getString("prenom"));
            }
        } catch (SQLException e) {
            System.out.println("Erreur lors de la lecture du ResultSet : "
                    + e.getMessage());
        } finally {
            System.out.println("Fermeture de l'objet ResultSet.");
            if (resultat != null) {
                try {
                    resultat.close();
                } catch (SQLException ignore) {
                }
            }
        }
        return l;
    }
    //INSERT INTO Users ()
    public void insertUser(String in_nom, String in_prenom, String in_mail, String in_mdp, String in_reponse,
                           String in_statut) {

        /* Exécution d'une requête d'écriture */
        int statut = execSQLWrite("INSERT INTO Users (nom, prenom, mail, mdp, reponse, statut) VALUES (\'" + in_nom + "\', \'" + in_prenom + "\',\' " + in_mail + "\', \' "+ in_mdp +"\', \'" + in_reponse + "\',\' " + in_statut + "\');");
        System.out.println("Statut de la requete : " +statut);
    }
}