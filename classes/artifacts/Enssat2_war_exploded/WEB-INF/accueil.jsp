<%--
  Created by IntelliJ IDEA.
  User: Guillaume
  Date: 28/03/2019
  Time: 15:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Accueil</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style type="text/css">
        .acc-form {
            width: 340px;
            margin: 50px auto;
        }
        .acc-form form {
            margin-bottom: 15px;
            background: #f7f7f7;
            box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
            padding: 30px;
        }
        .acc-form h2 {
            margin: 0 0 15px;
        }
        .form-control, .btn {
            min-height: 38px;
            border-radius: 2px;
        }
        .btn {
            font-size: 15px;
            font-weight: bold;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" > ATLAS : Gestion Informatique d'Etudiants </a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"><a>Accueil</a></li>
        </ul>
    </div>
</nav>

<div>
    <div class="acc-form">
        <form action="liste_etudiants" method="post">
            <button type="submit" class="btn btn-primary btn-block">Liste des étudiants</button>
        </form>
    </div>
</div>
<div>
    <div class="acc-form">
        <form action="liste_groupes" method="post">
            <button type="submit" class="btn btn-primary btn-block">Liste des groupes</button>
        </form>
    </div>
</div>
<div>
    <div class="acc-form">
        <form action="gerer_utilisateur" method="post">
            <button type="submit" class="btn btn-primary btn-block">Gérer les utilisateurs</button>
        </form>
    </div>
</div>

</body>
</html>


