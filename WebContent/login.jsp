
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Login Form</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<style type="text/css">
		.login-form {
			width: 340px;
			margin: 50px auto;
		}
		.login-form form {
			margin-bottom: 15px;
			background: #f7f7f7;
			box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
			padding: 30px;
		}
		.login-form h2 {
			margin: 0 0 15px;
		}
		.form-control, .btn {
			min-height: 38px;
			border-radius: 2px;
		}
		.btn {
			font-size: 15px;
			font-weight: bold;
		}
		.rightDiv{

		}
		.leftDiv{

		}
		.divCenter {
			display: flex;
			justify-content: center;
			padding: 40px;
		}
	</style>
</head>
<body>
<img src='<c:url value="/resources/background.jpg"/>' style='position:fixed;top:0px;left:0px;width:100%;height:100%;z-index:-1;' alt="">
<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="http://localhost:8080/Enssat2/login"> ATLAS : Gestion Informatique d'Etudiants </a>
		</div>
		<ul class="nav navbar-nav">
			<li class="active"><a>Login</a></li>
		</ul>
	</div>
</nav>
<input type="hidden" id="refresh" value="no">
	<script>
		window.onunload = function(){};
		$(document).ready(function(e) {
			// language=JQuery-CSS
			var $input = $('#refresh')
			$input.val() == 'yes' ? location.reload(true) : $input.val('yes');
		});
	</script>
<div class="divCenter">
	<div class="login-form leftDiv">
		<%-- Vérification de la présence d'un objet utilisateur en session --%>
			<c:choose>
				<%-- Si session détectée --%>
				<c:when test="${!empty sessionScope.sessionUtilisateur}">
				<form>
					<h2 class="text-center">Bonjour, ${sessionScope.sessionUtilisateur.prenom}</h2>
					<div class="form-group">
						<a href="http://localhost:8080/Enssat2/restreint/accueil" class="btn btn-primary btn-block" role="button">Accéder à mon espace</a>
					</div>
					<div class="form-group">
						<a href="http://localhost:8080/Enssat2/deconnexion" class="btn btn-warning btn-block" role="button">Déconnexion</a>
					</div>
				</form>
				</c:when>

				<%-- Si pas de session détectée --%>
				<c:otherwise>
				<form action="login" method="post">
					<h2 class="text-center">Se connecter</h2>
					<c:if test="${!empty form.erreurs}">
						<div class="alert alert-danger" role="alert">
							Utilisateur inconnu !
						</div>
					</c:if>
					<div class="form-group">
						<input id="email" name="email" type="email" class="form-control" placeholder="Utilisateur" required="required" />
					</div>
					<div class="form-group">
						<input id="password" name="password" type="password" class="form-control" placeholder="Mot de passe" required="required">
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-block">Log in</button>
					</div>
					<div class="form-group">
						<a href="http://localhost:8080/Enssat2/changePassword"> Mot de passe oublié ?</a>
					</div>
				</form>
				</c:otherwise>
			</c:choose>
	</div>
</div>
</body>