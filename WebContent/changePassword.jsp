<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mdp Form</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style type="text/css">
        body{
            height:100%;
            width:100%;
            background-image:url(/resources/background.jpg);/*your background image*/
            background-repeat:no-repeat;/*we want to have one single image not a repeated one*/
            background-size:cover;/*this sets the image to fullscreen covering the whole screen*/
            /*css hack for ie*/
            filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='.resources/background.jpg', sizingMethod=scale);
            -ms-filter:"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='image.jpg',sizingMethod='scale')";
        }
        .mdp-form {
            width: 400px;
            margin: 50px auto;
        }
        .mdp-form form {
            margin-bottom: 15px;
            background: #f7f7f7;
            box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
            padding: 30px;
        }
        .mdp-form h2 {
            margin: 0 0 15px;
        }
        .form-control, .btn {
            min-height: 38px;
            border-radius: 2px;
        }
        .btn {
            font-size: 15px;
            font-weight: bold;
        }
    </style>
</head>
<body>
<img src='<c:url value="/resources/background.jpg"/>' style='position:fixed;top:0px;left:0px;width:100%;height:100%;z-index:-1;' alt="">
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="http://localhost:8080/Enssat2/login" > ATLAS : gestion informatique d'étudiants </a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="http://localhost:8080/Enssat2/login">Login</a></li>
        </ul>
        <ul class="nav navbar-nav">
            <li class="active"><a>Changement du mot de passe</a></li>
        </ul>
    </div>
</nav>
<div class="mdp-form">
    <form action="changePassword" method="post">
        <h2 class="text-center">Remplissez le formulaire</h2>
        <c:if test="${!empty form.erreurs}">
            <div class="alert alert-danger" role="alert">
                Saisie incorrecte !
            </div>
        </c:if>
        <div class="form-group">
            <input id="email" name="email" type="text" class="form-control" placeholder="Utilisateur" required="required">
        </div>
        <div class="form-group">
            <p> Question : <b>quelle est votre couleur préférée ?</b> </p>
        </div>
        <div class="form-group">
            <input id="reponse" name="reponse" type="text" class="form-control" placeholder="Réponse" required="required">
        </div>
        <div class="form-group">
            <input id="password" name="password" type="password" class="form-control" placeholder="Nouveau mot de passe" required="required">
        </div>
        <div class="form-group">
            <input id="password2" name="password2" type="password"  class="form-control" placeholder="Confirmer mot de passe" required="required">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-secondary btn-block">Changer mon mot de passe</button>
        </div>
    </form>
</div>
</body>
</html>