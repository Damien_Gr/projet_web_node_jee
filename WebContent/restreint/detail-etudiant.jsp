<%@ page pageEncoding="UTF-8" %>
<%@ page import="Etudiants.Etudiants" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.List" %>
<html>
<head>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <script src="../js/bootstrap.min.js"></script>
</head>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="http://localhost:8080/Enssat2/login" > ATLAS : gestion informatique d'étudiants </a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="http://localhost:8080/Enssat2/login">Login</a></li>
        </ul>
        <ul class="nav navbar-nav">
            <li><a href="http://localhost:8080/Enssat2/restreint/accueil">Accueil</a></li>
        </ul>
        <ul class="nav navbar-nav">
            <li class="active"><a>Detail etudiant</a></li>
        </ul>
    </div>
</nav>

<body>
<div class="container">
    <table class="table table-striped">
        <thead>
        <tr>
            <td></td>
            <td>Nom</td>
            <td>Prénom</td>
            <td>Date de naissance</td>
            <td>Courriel professionnel</td>
            <td>Courriel personnel</td>
            <td>Série baccalauréat</td>
            <td>Date du baccalauréat</td>
            <td>Mention au baccalauréat</td>
            <td>Diplôme </td>
            <td>Date diplôme</td>
            <td>Ville diplôme</td>
        </tr>
        </thead>

        <%
            System.out.println("ezieghilmknikhkd");
            Etudiants Etu = (Etudiants) request.getAttribute("ETU");
            System.out.println(Etu);


            int idUtilisateur = Etu.getIdUtilisateur();
            String nom = Etu.getNom();
            String prenom = Etu.getPrenom();
            String dateNaissance =Etu.getDateNaissance();
            String courielPro =Etu.getCourielPro();
            String courielPerso =Etu.getCourielPerso();
            String serieBac =Etu.getSerieBac();
            String dateBac=Etu.getDateBac();
            String mentionBac = Etu.getMentionBac();
            String diplome = Etu.getDiplome();
            String dateDiplome =Etu.getDateDiplome();
            String villeDiplome = Etu.getVilleDiplome();
        %>

        <tr>

            <td ><Input type="hidden" name="ID" value=<%=idUtilisateur%>></td>
            <td><%=nom %></td>
            <td><%=prenom %></td>
            <td><%=dateNaissance %></td>
            <td><%=courielPro %></td>
            <td><%= courielPerso%></td>
            <td><%=serieBac %></td>
            <td><%= dateBac %></td>
            <td><%= mentionBac %></td>
            <td><%= diplome %></td>
            <td><%= dateDiplome %></td>
            <td><%= villeDiplome %></td>
        </tr>

        <%  // close loop
        %>

    </table>
    <div class="acc-form">
        <form action="delete-etudiant" method="post">
            <Input type="hidden" name="ID" value=<%=idUtilisateur%>>
            <button type="submit" class="btn btn-primary btn-block" title="Supprimer" >Supprimer</button></form>
        <form action="modif-etudiant" method="post">
            <Input type="hidden" name="ID" value=<%=idUtilisateur%>>
            <button type="submit" class="btn btn-primary btn-block" title="Details" onclick="document.getElementById(ID)">Modifier</button></form>
        <a role="button" href="http://localhost:8080/Enssat2/restreint/liste_etudiants" class="btn btn-primary btn-block">Retour à la liste</a>
    </div>
</div>
</body>
</html>
