<%@ page pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mdp Form</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style type="text/css">
        .mdp2-form {
            width: 340px;
            margin: 50px auto;
        }
        .mdp2-form form {
            margin-bottom: 15px;
            background: #f7f7f7;
            box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
            padding: 30px;
        }
        .mdp2-form h2 {
            margin: 0 0 15px;
        }
        .form-control, .btn {
            min-height: 38px;
            border-radius: 2px;
        }
        .btn {
            font-size: 15px;
            font-weight: bold;
        }
    </style>
</head>

<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="http://localhost:8080/Enssat2/login" > ATLAS : gestion informatique d'étudiants </a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="http://localhost:8080/Enssat2/login">Login</a></li>
        </ul>
        <ul class="nav navbar-nav">
            <li><a href="http://localhost:8080/Enssat2/restreint/accueil">Accueil</a></li>
        </ul>
        <ul class="nav navbar-nav">
            <li class="active"><a>Création d'étudiants</a></li>
        </ul>
    </div>
</nav>
<div class="mdp2-form">
    <h2 class="text-center">Etudiant créé avec succès</h2>
    <form action="${pageContext.request.contextPath}/restreint/liste_etudiants" method="get">
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block">Retour</button>
        </div>
    </form>
</div>
</body>
</html>