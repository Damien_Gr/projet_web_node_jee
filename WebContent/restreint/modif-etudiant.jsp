<%@ page import="Etudiants.Etudiants" %>
<%@ page pageEncoding="UTF-8" %>
<html>
<head>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <script src="../js/bootstrap.min.js"></script>
</head>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="http://localhost:8080/Enssat2/login" > ATLAS : gestion informatique d'étudiants </a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="http://localhost:8080/Enssat2/login">Login</a></li>
        </ul>
        <ul class="nav navbar-nav">
            <li><a href="http://localhost:8080/Enssat2/restreint/accueil">Accueil</a></li>
        </ul>
        <ul class="nav navbar-nav">
            <li class="active"><a>Modification d'etudiant</a></li>
        </ul>
    </div>
</nav>

<body>
<div class="container">
    <form action="modifeffectuee" method="post"  role="form" data-toggle="validator" >
        <c:if test ="${empty action}">
            <c:set var="action" value="guardar"/>
        </c:if>

                <%
            System.out.println("ezieghilmknikhkd");
            Etudiants Etu = (Etudiants) request.getAttribute("ETU");
            System.out.println(Etu);


            int idUtilisateur = Etu.getIdUtilisateur();
            String nom = Etu.getNom();
            String prenom = Etu.getPrenom();
            String dateNaissance =Etu.getDateNaissance();
            String courielPro =Etu.getCourielPro();
            String courielPerso =Etu.getCourielPerso();
            String serieBac =Etu.getSerieBac();
            String dateBac=Etu.getDateBac();
            String mentionBac = Etu.getMentionBac();
            String diplome = Etu.getDiplome();
            String dateDiplome =Etu.getDateDiplome();
            String villeDiplome = Etu.getVilleDiplome();
        %>

        <input type="hidden" id="action" name="action" value="${action}">
        <input type="hidden" id="idEstudiante" name="idEstudiante" value=<%=idUtilisateur%>
        <h2>Modification Etudiant</h2>
        <div class="form-group">
            <label for="nom" class="control-label col-xs-4">Nom:</label>
            <input type="text" name="nom" id="nom" class="form-control" placeholder=<%=nom%> required="true" />

            <label for="prenom" class="control-label col-xs-4">Prénom:</label>
            <input type="text" name="prenom" id="prenom" class="form-control" placeholder=<%=prenom%> required="true"/>

            <label for="dateNaissance" class="control-label col-xs-4">Date de naissance</label>
            <input type="text"  name="dateNaissance" id="dateNaissance" class="form-control" placeholder=<%=dateNaissance%> required="true"/>

            <label for="courielPro" class="control-label col-xs-4">Courriel Pro:</label>
            <input type="text" name="courielPro" id="courielPro" class="form-control" placeholder=<%=courielPro%> required="true"/>

            <label for="courielPerso" class="control-label col-xs-4">Courriel Perso:</label>
            <input type="text" name="courielPerso" id="courielPerso" class="form-control"  placeholder=<%=courielPerso%> required="true"/>

            <label for="serieBac" class="control-label col-xs-4">Série Bac:</label>
            <input type="text" name="serieBac" id="serieBac" class="form-control"  placeholder=<%=serieBac%> required="true"/>

            <label for="dateBac" class="control-label col-xs-4">Date Bac:</label>
            <input type="text" name="dateBac" id="dateBac" class="form-control"  placeholder=<%=dateBac%> required="true"/>

            <label for="mentionBac" class="control-label col-xs-4">Mention Bac:</label>
            <input type="text" name="mentionBac" id="mentionBac" class="form-control"  placeholder=<%=mentionBac%> required="true"/>

            <label for="diplome" class="control-label col-xs-4">Diplôme:</label>
            <input type="text" name="diplome" id="diplome" class="form-control"  placeholder=<%=diplome%> required="true"/>

            <label for="dateDiplome" class="control-label col-xs-4">Date diplôme:</label>
            <input type="text" name="dateDiplome" id="dateDiplome" class="form-control" placeholder=<%=dateDiplome%> required="true"/>

            <label for="villeDiplome" class="control-label col-xs-4">Ville diplôme:</label>
            <input type="text" name="villeDiplome" id="villeDiplome" class="form-control"  placeholder=<%=villeDiplome%> required="true"/>


            <br></br>
            <button type="submit" class="btn btn-primary">Enregistrer</button>

</div>
</body>
</html>