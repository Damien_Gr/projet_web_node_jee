<%@ page pageEncoding="UTF-8" %>
<html>
<head>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <script src="../js/bootstrap.min.js"></script>
</head>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="http://localhost:8080/Enssat2/login" > ATLAS : gestion informatique d'étudiants </a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="http://localhost:8080/Enssat2/login">Login</a></li>
        </ul>
        <ul class="nav navbar-nav">
            <li><a href="http://localhost:8080/Enssat2/restreint/accueil">Accueil</a></li>
        </ul>
        <ul class="nav navbar-nav">
            <li class="active"><a>Création de groupe</a></li>
        </ul>
    </div>
</nav>

<body>
<div class="container">
    <form action="groupe-cree" method="post"  role="form" data-toggle="validator" >
        <c:if test ="${empty action}">
            <c:set var="action" value="guardar"/>
        </c:if>
        <input type="hidden" id="action" name="action" value="${action}">
        <input type="hidden" id="idEstudiante" name="idEstudiante" value="${estudiante.id}">
        <h2>Nouveau Groupe</h2>
        <div class="form-group">
            <label for="nombre" class="control-label col-xs-4">ID:</label>
            <input type="text" name="nombre" id="nombre" class="form-control" value="${estudiante.nombre}" required="true" />

            <label for="apellido" class="control-label col-xs-4">Nom:</label>
            <input type="text" name="apellido" id="apellido" class="form-control" value="${estudiante.apellido}" required="true"/>


            <br></br>
            <button type="submit" class="btn btn-primary">Enregistrer</button>
        </div>
    </form>
</div>
</body>
</html>