<%@ page pageEncoding="UTF-8" %>
<html>
    <head>
        <link rel="stylesheet" href="../css/bootstrap.min.css">
        <script src="../js/bootstrap.min.js"></script>
    </head>

    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="http://localhost:8080/Enssat2/login" > ATLAS : gestion informatique d'étudiants </a>
            </div>
            <ul class="nav navbar-nav">
                <li><a href="http://localhost:8080/Enssat2/login">Login</a></li>
            </ul>
            <ul class="nav navbar-nav">
                <li><a href="http://localhost:8080/Enssat2/restreint/accueil">Accueil</a></li>
            </ul>
            <ul class="nav navbar-nav">
                <li class="active"><a>Création d'étudiant</a></li>
            </ul>
        </div>
    </nav>

    <body>
        <div class="container">
            <form action="${pageContext.request.contextPath}/restreint/nouveletudiant" method="post" >
                <h2>Nouvel Etudiant</h2>
                <div class="form-group">
                    <label for="nom" class="control-label col-xs-4">Nom:</label>
                    <input type="text" name="nom" id="nom" class="form-control" required="true" />

                    <label for="prenom" class="control-label col-xs-4">Prénom:</label>
                    <input type="text" name="prenom" id="prenom" class="form-control" required="true"/>

                    <label for="dateNaissance" class="control-label col-xs-4">Date de naissance</label>
                    <input type="text"  pattern="^\d{2}-\d{2}-\d{4}$" name="dateNaissance" id="dateNaissance" class="form-control" maxlength="10" placeholder="dd-MM-yyy" required="true"/>

                    <label for="courielPro" class="control-label col-xs-4">Courriel Pro:</label>
                    <input type="text" name="courielPro" id="courielPro" class="form-control" required="true"/>

                    <label for="courielPerso" class="control-label col-xs-4">Courriel Perso:</label>
                    <input type="text" name="courielPerso" id="courielPerso" class="form-control"  required="true"/>

                    <label for="serieBac" class="control-label col-xs-4">Série Bac:</label>
                    <input type="text" name="serieBac" id="serieBac" class="form-control"  required="true"/>

                    <label for="dateBac" class="control-label col-xs-4">Date Bac:</label>
                    <input type="text" name="dateBac" id="dateBac" class="form-control"  required="true"/>

                    <label for="mentionBac" class="control-label col-xs-4">Mention Bac:</label>
                    <input type="text" name="mentionBac" id="mentionBac" class="form-control"   required="true"/>

                    <label for="diplome" class="control-label col-xs-4">Diplôme:</label>
                    <input type="text" name="diplome" id="diplome" class="form-control"   required="true"/>

                    <label for="dateDiplome" class="control-label col-xs-4">Date diplôme:</label>
                    <input type="text" name="dateDiplome" id="dateDiplome" class="form-control" required="true"/>

                    <label for="villeDiplome" class="control-label col-xs-4">Ville diplôme:</label>
                    <input type="text" name="villeDiplome" id="villeDiplome" class="form-control"  required="true"/>

                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                </div>                                                      
            </form>
        </div>
    </body>
</html>