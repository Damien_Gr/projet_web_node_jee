<html>
<head>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <script src="../js/bootstrap.min.js"></script>
</head>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="http://localhost:8080/Enssat2/login" > ATLAS : gestion informatique d'étudiants </a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="http://localhost:8080/Enssat2/login">Login</a></li>
        </ul>
        <ul class="nav navbar-nav">
            <li><a href="http://localhost:8080/Enssat2/restreint/accueil">Accueil</a></li>
        </ul>
        <ul class="nav navbar-nav">
            <li class="active"><a>Modification d'utilisateur</a></li>
        </ul>
    </div>
</nav>

<body>
<div class="container">

    <form action="modif-utilisateur" method="post"  role="form" data-toggle="validator" >
        <c:if test ="${empty action}">
            <c:set var="action" value="guardar"/>
        </c:if>

        <input type="hidden" id="action" name="action" value="${action}">
        <input type="hidden" id="idEstudiante" name="idEstudiante" value="${estudiante.id}">
        <h2>Nouvel Etudiant</h2>
        <div class="form-group">
            <label for="nombre" class="control-label col-xs-4">Nom:</label>
            <input type="text" name="nombre" id="nombre" class="form-control" value="${estudiante.nombre}" required="true" />

            <label for="apellido" class="control-label col-xs-4">Prénom:</label>
            <input type="text" name="apellido" id="apellido" class="form-control" value="${estudiante.apellido}" required="true"/>

            <label for="fnacimiento" class="control-label col-xs-4">Date de naissance</label>
            <input type="text"  pattern="^\d{2}-\d{2}-\d{4}$" name="fnacimiento" id="fnacimiento" class="form-control" value="${estudiante.fechaNacimiento}" maxlength="10" placeholder="dd-MM-yyy" required="true"/>

            <label for="carrera" class="control-label col-xs-4">Courriel Pro:</label>
            <input type="text" name="carrera" id="carrera" class="form-control" value="${estudiante.carrera}" required="true"/>

            <label for="semestre" class="control-label col-xs-4">Courriel Perso:</label>
            <input type="text" name="semestre" id="semestre" class="form-control" value="${estudiante.semestre}" required="true"/>

            <label for="email" class="control-label col-xs-4">Série Bac:</label>
            <input type="text" name="email" id="email" class="form-control" value="${estudiante.email}"  required="true"/>

            <label for="email" class="control-label col-xs-4">Date Bac:</label>
            <input type="text" name="email" id="email" class="form-control" value="${estudiante.email}"  required="true"/>

            <label for="email" class="control-label col-xs-4">Mention Bac:</label>
            <input type="text" name="email" id="email" class="form-control" value="${estudiante.email}"  required="true"/>

            <label for="email" class="control-label col-xs-4">Diplôme:</label>
            <input type="text" name="email" id="email" class="form-control" value="${estudiante.email}"  required="true"/>

            <label for="email" class="control-label col-xs-4">Date diplôme:</label>
            <input type="text" name="email" id="email" class="form-control" value="${estudiante.email}" required="true"/>

            <label for="email" class="control-label col-xs-4">Ville diplôme:</label>
            <input type="text" name="email" id="email" class="form-control" value="${estudiante.email}"  required="true"/>


            <br></br>
            <button type="submit" class="btn btn-primary">Enregistrer</button>
        </div>
    </form>
</div>
</body>
</html>