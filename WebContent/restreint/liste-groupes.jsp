<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

    <title>Liste des Groupes</title>
</head>
<body>
<link rel="stylesheet" href="../css/bootstrap.min.css">
<script src="../js/bootstrap.min.js"></script>
</body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="http://localhost:8080/Enssat2/login" > ATLAS : gestion informatique d'�tudiants </a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="http://localhost:8080/Enssat2/login">Login</a></li>
        </ul>
        <ul class="nav navbar-nav">
            <li><a href="http://localhost:8080/Enssat2/restreint/accueil">Accueil</a></li>
        </ul>
        <ul class="nav navbar-nav">
            <li class="active"><a>Liste des groupes</a></li>
        </ul>
    </div>
</nav>

<div class="container">
    <h2>Groupes</h2>

    <form action="RechercheGroupe" method="get" id="rechercher"
          role="form">
        <input type="hidden" id="recherche" name="recherche"
               value="rechercheParNom">
        <div class="form-group col-xs-5">
            <input type="text" name="nomGroupe" id="nomGroupe"
                   class="form-control" required="true"
                   placeholder="Nom du groupe" />
        </div>
        <button type="submit" class="btn btn-info">
            <span class="glyphicon glyphicon-search"></span> Recherche
        </button>
    </form>

    <div class="container">
        <form action="nouveaugroupe" method="post">
            <input type="hidden" id="recherche_" name="recherche"
                   value="nouveau"> <br></br>
            <button type="submit" class="btn btn-primary  btn-md pull-left">Nouveau
                groupe</button>
        </form>


    </div>
    <br></br>

    <c:if test="${not empty message}">
       <!-- <div class="alert alert-success">${message}</div>  -->
    </c:if>
    <form action="coucou" method="post" id="EtudiantsForm" role="form">
        <input type="hidden" id="idEtudiants" name="idEtudiants"> <input
            type="hidden" id="action" name="action">
        <c:choose>
            <c:when test="${not empty listaEtudiantss}">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <td>ID</td>
                        <td>Nom</td>
                    </tr>
                    </thead>
                    <c:forEach var="Etudiants" items="${listaEtudiantss}">
                        <c:set var="classSucess" value="" />
                        <c:if test="${idEtudiant == etudiant.id}">
                            <c:set var="classSucess" value="info" />
                        </c:if>
                        <tr class="${classSucess}">
                            <td><a
                                    href="Etudiants?idEtudiants=${Etudiants.id}&buscarAction=buscarPorId">${Etudiants.idUtilisateur[0]}</a></td>
                            <td>${Groupes.id[0]}</td>
                            <td>${Groupes.nom[0]}</td>

                            <td><a href="#" id="supprimer"
                                   onclick="document.getElementById('idUtilisateur').value='${Etudiants.idUtilisateur}';
                                           document.getElementById('action').value='supprimer';
                                           document.getElementById('EtudiantsForm').submit();">
                                <span class="glyphicon glyphicon-trash" />
                            </a></td>
                        </tr>
                    </c:forEach>
                </table>
            </c:when>
            <c:otherwise>
                <br>
                <div class="alert alert-info">
                    Il n'y a pas de groupes qui correspondent � la recherche</div>
            </c:otherwise>
        </c:choose>
    </form>
</div>
</html>