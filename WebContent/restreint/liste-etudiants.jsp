<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="Etudiants.Etudiants" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.List" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Liste des Etudiants</title>
</head>
<body>
	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<script src="../js/bootstrap.min.js"></script>
</body>

<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="http://localhost:8080/Enssat2/login" > ATLAS : gestion informatique d'étudiants </a>
		</div>
		<ul class="nav navbar-nav">
			<li><a href="http://localhost:8080/Enssat2/login">Login</a></li>
		</ul>
		<ul class="nav navbar-nav">
			<li><a href="http://localhost:8080/Enssat2/restreint/accueil">Accueil</a></li>
		</ul>
		<ul class="nav navbar-nav">
			<li class="active"><a>Liste des étudiants</a></li>
		</ul>
	</div>
</nav>

<div class="container">
	<h2>Etudiants</h2>

	<form action="RechercheEtudiant" method="get" id="rechercher"
		role="form">
		<input type="hidden" id="recherche" name="recherche"
			value="rechercheParNom">
		<div class="form-group col-xs-5">
			<input type="text" name="nomEtudiant" id="nomEtudiant"
				class="form-control" required="true"
				placeholder="Nom de l'étudiant" />
		</div>
		<button type="submit" class="btn btn-info">
			<span class="glyphicon glyphicon-search"></span> Recherche
		</button>
	</form>

	<div class="container">
		<form action="http://localhost:8080/Enssat2/restreint/nouveletudiant" method="get">
			<input type="hidden" id="recherche_" name="recherche"
				   value="nouveau">
			<button type="submit" class="btn btn-primary  btn-md pull-left">Nouvel étudiant</button>
		</form>


	</div>
	<br></br>





	<c:if test="${not empty message}">
		<!-- <div class="alert alert-success">${message}</div> -->
	</c:if>

		<c:choose>


			<c:when test="true">
				<table class="table table-striped">
					<thead>
					<tr>
						<td></td>
						<td>Nom</td>
						<td>Prénom</td>
						<td>Date de naissance</td>
					</tr>
					</thead>

					<%
						System.out.println("ezieghilmknikhkd");
						List<Etudiants> ListEtu = (List)request.getAttribute("EtuList");
						System.out.println(ListEtu);


					Iterator<Etudiants> contacts = ListEtu.iterator();
						while (contacts.hasNext()) {
							Etudiants Etu = contacts.next();

							int idUtilisateur = Etu.getIdUtilisateur();
							String nom = Etu.getNom();
							String prenom = Etu.getPrenom();
							String dateNaissance =Etu.getDateNaissance();
							System.out.println("Id utilisateur" + idUtilisateur);
					%>

					<tr>
						<form action="detail-etudiant" method="post">
						<td ><Input type="hidden" name="ID" value=<%=idUtilisateur%>></td>
						<td><%=nom %></td>
						<td><%=prenom %></td>
						<td><%=dateNaissance %></td>
						<td><button type="submit" class="btn btn-primary btn-block" title="Details" onclick="document.getElementById(ID)">Details</button></form></td>

					</tr>

					<% } // close loop
					%>

				</table>
			</c:when>
			<c:otherwise>
				<br>
				<div class="alert alert-info">
					Il n'y a pas d'étudiants qui correspondent à la recherche</div>
			</c:otherwise>
		</c:choose>

</div>
</html>